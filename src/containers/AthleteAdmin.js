import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router'
import Indicator from '../components/Indicator'
import firebase from '../config/database'
import Fields from '../components/fields/Fields.js'
import Radio from '../components/fields/Radio.js'
import File from '../components/fields/File.js'
import Input from '../components/fields/Input.js';
import Select from '../components/fields/Select.js';
import CheckBox from '../components/fields/CheckBox.js';
import SelectMeet from '../components/fields/SelectMeet.js';
import KreedDatePicker from '../components/fields/KreedDatePicker.js';
import Table from '../components/tables/Table.js'
import DocsTable from '../components/tables/DocsTable.js'
import Config from '../config/app';
import Common from '../common.js';
import Notification from '../components/Notification';
import SkyLight from 'react-skylight';
import INSERT_STRUCTURE from "../config/firestoreschema.js"
import FirebasePaginator from "firebase-paginator"
import NavBar from '../components/NavBar'
import moment from 'moment';
import Image from '../components/fields/Image.js';
import FileUploader from 'react-firebase-file-uploader';
import SweetAlert from 'react-bootstrap-sweetalert';
import * as firebaseREF from 'firebase';
import KreedImage from '../components/fields/KreedImage.js';
import { SSL_OP_CRYPTOPRO_TLSEXT_BUG } from 'constants';
//import trim from 'trim';
require("firebase/firestore");

//for multiselect
import FilteredMultiSelect from 'react-filtered-multiselect'

// for file uploader
const uuidv1 = require('uuid/v1');

const ROUTER_PATH = "/athlete/";
var Loader = require('halogen/PulseLoader');



class AthleteAdmin extends Component {
  constructor(props) {
    super(props);

    //Create initial step
    this.state = {
      fields: {}, //The editable fields, textboxes, checkbox, img upload etc..
      arrays: {}, //The array of data
      elements: [], //The elements - objects to present
      elementsInArray: [], //The elements put in array
      directValue: "", //Direct access to the value of the current path, when the value is string
      firebasePath: "",
      arrayNames: [],
      currentMenu: {},
      completePath: "",
      lastSub: "",
      isJustArray: false,
      currentInsertStructure: null,
      notifications: [],
      lastPathItem: "",
      pathToDelete: null,
      isItArrayItemToDelete: false,
      page: 1,
      documents: [],
      collections: [],
      currentCollectionName: "",
      isCollection: false,
      isDocument: false,
      keyToDelete: null,
      theSubLink: null,
      fieldsOfOnsert: null,
      isLoading: false,
      showAddCollection: "test",
      dlProgress: new Map(),

      debounce: false,
      userinfo: [],
      club_id: "",
      // new athlete
      full_name: "",
      date_of_birth: "",
      gender: "",
      mobile_number: "",
      email: "",
      parent_name: "",
      parent_mobile_number:"",
      address: "",
      city: "Bangalore",
      pin_code: "",
      school_name: "",
      school_code: "",
      school_mobile_number: "",
      school_email:"",
      club_name:"",
      swimGroup:"",
      events:"",
      eventList: [],
      selectedEvents:[],


      attachments: [],
      editedFields: [],
      clubCoaches: [],
      errors: {},
      username: '',//file upload button
      avatar: '',
      isUploading: false,
      dobEntered: false,
      progress: 0,
      avatarURL: '',
      links: [],
      swimGroup: "",
      docDir: "",


      errorStatement:"",
      errorAlert: false,
      phoneAlert: false,
      loadingAlert: false,
      deleteAlert: false,
    };

    //Bind function to this

    this.getCollectionDataFromFireStore = this.getCollectionDataFromFireStore.bind(this);
    this.resetDataFunction = this.resetDataFunction.bind(this);
    this.processRecords = this.processRecords.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.cancelDelete = this.cancelDelete.bind(this);
    this.cancelAddFirstItem = this.cancelAddFirstItem.bind(this);
    this.doDelete = this.doDelete.bind(this);
    this.deleteFieldAction = this.deleteFieldAction.bind(this);
    this.refreshDataAndHideNotification = this.refreshDataAndHideNotification.bind(this);
    this.addKey = this.addKey.bind(this);
    this.showSubItems = this.showSubItems.bind(this);
    this.updatePartOfObject = this.updatePartOfObject.bind(this);
    this.addDocumentToCollection = this.addDocumentToCollection.bind(this);
    this.addItemToArray = this.addItemToArray.bind(this);
    this.addNewAthlete = this.addNewAthlete.bind(this);
    this.formValueCapture = this.formValueCapture.bind(this);
    this.ionViewDidLoad = this.ionViewDidLoad.bind(this);

    
    this.saveEdits = this.saveEdits.bind(this);
    this.resetEdits = this.resetEdits.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.cancelAddNewAthlete = this.cancelAddNewAthlete.bind(this);



    this.prepAddNew = this.prepAddNew.bind(this);

    //Alerts related
    this.hideAlert = this.hideAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);

   

    //groups and events
    this.getGroup = this.getGroup.bind(this);
    this.getEventList = this.getEventList.bind(this);
  }

  

  //sweet alerts for delete data
  showAlert(name) {
    this.setState({ [name]: true });
  }
  hideAlert(name) {
    this.setState({ [name]: false });
  }

  //end for displaying file list
  ionViewDidLoad() {
    this.userId = firebase.auth().currentUser.uid
    alert(this.userId);
  }

  //ionViewDidLoad();
  /**
   * Step 0a
   * Start getting data
   */
  componentDidMount() {
    this.findFirestorePath();
    //this.getMeTheFirestorePath();
    window.sidebarInit();
  }
  //setting date of birth of athlete to -5 years from today date
  // startDOB() {

  //   return new Date(new Date().setFullYear(new Date().getFullYear() - 5));
  // }
  /**
  * Step 0b
  * Resets data function
  */
  resetDataFunction() {
    var newState = {};
    newState.documents = [];
    newState.collections = [];
    newState.currentCollectionName = "";
    newState.fieldsAsArray = [];
    newState.arrayNames = [];
    newState.fields = [];
    newState.arrays = [];
    newState.elements = [];
    newState.elementsInArray = [];
    newState.theSubLink = null;

    newState.full_name = "";
    newState.date_of_birth = "";
    newState.gender = "";
    newState.mobile_number = "";
    newState.email = "";
    newState.parent_name = "";
    newState.parent_mobile_number = "";
    newState.address = "";
    newState.city = "";
    newState.pin_code = "";
    newState.school_name = "";
    newState.school_code = "";
    newState.school_mobile_number = "";
    newState.school_email = "";
    newState.club_name = ""; 
    newState.swimGroup = ""; 
    newState.selectedEvents = [];

    this.resetEdits();

    this.setState(newState);
    //this.findFirestorePath();
    this.getMeTheFirestorePath();
  }

  
  /**
   * Step 0c
   * componentWillReceiveProps event of React, fires when component is mounted and ready to display
   * Start connection to firebase
   */
  componentWillReceiveProps(nextProps, nextState) {
    console.log("Next SUB: " + nextProps.params.sub);
    console.log("Prev SUB : " + this.props.params.sub);
    if (nextProps.params.sub == this.props.params.sub) {
      console.log("update now");
      this.setState({ isLoading: true })
      this.resetDataFunction();
    }
  }

  /**
   * Step 0d
   * getMeTheFirestorePath created firestore path based on the router parh
   */
  getMeTheFirestorePath() {
    // this.userId = firebase.app.auth().currentUser.uid;
    // var db = firebase.app.firestore();
    //var docRef = db.collection("clubmaps").doc("user" + this.userId);
    // docRef.get().then(doc => {
    //   if (doc.exists) {
    //     var pathdoc = doc.data();
    //     this.findFirestorePath(pathdoc.dbpath);
    //   }
    //   else { alert("error"); }
    // })

    var thePath=(this.props.route.path.replace(ROUTER_PATH,"").replace(":sub",""))+(this.props.params&&this.props.params.sub?this.props.params.sub:"").replace(/\+/g,"/");;
    return thePath;
  }

    // to get group
    getGroup(dob) {
      var timestamp = Date.parse(dob);
  
      if (isNaN(timestamp))
        return "Open";
  
      var current = new Date();
      var dob_dt = new Date(dob);
      var today = current.getFullYear() - dob_dt.getFullYear();
  
      if (today > 19) {
        return "Ages abouve 18 are not permitted";
      } else if (today == 19 || today == 18) {
        return "Group U19";
      } else if (today == 17 || today == 16 || today == 15 || today == 14 || today == 13 || today == 12 
                 || today == 11 || today == 10 || today == 9|| today == 8  ) {
        return "Group U17";
      } else {
        return "Kids below 7 years of age are not permitted."
      }
    }

    // to get event list
    getEventList() {
      var events = [];
  
  
      if (this.state.swimGroup == "Group U17" || this.state.swimGroup == "Group U19" ) 
      {
        events = ["50m FreeStyle", "100m FreeStyle", "50m BackStroke", "50m BreastSroke",
                  "100m BreastStroke", "50m Butterfly", "100m Butterfly"];
      } 

      
      this.setState({ eventList: events })
    }
  /**
   * Step 1
   * Finds out the Firestore path
   * Also creates the path that will be used to access the insert
   */
  findFirestorePath() {
    var pathData = {}
    if (this.props.params && this.props.params.sub) {
      pathData.lastSub = this.props.params.sub;
    }

    //Find the firestore path
    var firebasePath = this.getMeTheFirestorePath();
    pathData.firebasePath = firebasePath;

    //Find last path - the last item
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    pathData.lastPathItem = Common.capitalizeFirstLetter(items[items.length - 1]);
    pathData.completePath = subPath;

    //Save this in state
    this.setState(pathData);

    //Go to next step of finding the collection data
    this.getCollectionDataFromFireStore(firebasePath);
  }



  /**
  * Step 2
  * Connect to firestore to get the current item we need
  * @param {String} collection - this infact can be collection or document
  */
 getCollectionDataFromFireStore(collection) {

  //Create the segmments based on the path / collection we have
  var segments = collection.split("/");
  var lastSegment = segments[segments.length - 1];

  //Is this a call to a collections data
  var isCollection = segments.length % 2;

  //Reference to this
  var _this = this;

  //Save know info for now
  this.setState({
    currentCollectionName: segments[segments.length - 1],
    isCollection: isCollection,
    isDocument: !isCollection,
  })

  //Get reference to firestore
  var db = firebase.app.firestore();

  //Here, we will save the documents from collection
  var documents = [];

  if (isCollection) {
    //COLLECTIONS - GET DOCUMENTS 

    db.collection(collection).orderBy("full_name").get()
      .then(function (querySnapshot) {
        var datacCount = 0;
        querySnapshot.forEach(function (doc) {

          //Increment counter
          datacCount++;

          //Get the object
          var currentDocument = doc.data();

          //Sace uidOfFirebase inside him
          currentDocument.uidOfFirebase = doc.id;

          console.log(doc.id, " => ", currentDocument);

          //Save in the list of documents
          documents.push(currentDocument)
        });
        console.log("DOCS----");
        console.log(documents);

        //Save the douments in the sate
        _this.setState({
          isLoading: false,
          documents: documents,
          showAddCollection: datacCount == 0 ? collection : ""
        })
        if (datacCount == 0) {
          _this.refs.addCollectionDialog.show();
        }
        console.log(_this.state.documents);
      });
  } else {
    //DOCUMENT - GET FIELDS && COLLECTIONS
    var referenceToCollection = collection.replace("/" + lastSegment, "");

    //Create reference to the document itseld
    var docRef = db.collection(referenceToCollection).doc(lastSegment);

    //Get the starting collectoin
    var parrentCollection = segments;
    parrentCollection.splice(-1, 1);

    //Find the collections of this document
    this.findDocumentCollections(parrentCollection);

    docRef.get().then(function (doc) {
      if (doc.exists) {
        console.log("Document data:", doc.data());

        //Directly process the data
        _this.processRecords(doc.data())
      } else {
        console.log("No such document!");
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });
  }
}

  /**
   * Step 3
   * findDocumentCollections - what collections should we display / currently there is no way to get collection form docuemnt
   * @param {Array} chunks - the collection / documents
   */
  findDocumentCollections(chunks) {

    console.log("Search for the schema now of " + chunks);

    //At start is the complete schema
    var theInsertSchemaObject = INSERT_STRUCTURE;
    var cuurrentFields = null;
    console.log("CHUNKS");
    console.log(chunks);

    //Foreach chunks, find the collections / fields
    chunks.map((item, index) => {
      console.log("current chunk:" + item);

      //Also make the last object any
      //In the process, check if we have each element in our schema
      if (theInsertSchemaObject != null && theInsertSchemaObject && theInsertSchemaObject[item] && theInsertSchemaObject[item]['collections']) {
        var isLastObject = (index == (chunks.length - 1));

        if (isLastObject && theInsertSchemaObject != null && theInsertSchemaObject[item] && theInsertSchemaObject[item]['fields']) {
          cuurrentFields = theInsertSchemaObject[item]['fields'];
        }

        if (isLastObject && theInsertSchemaObject != null) {
          //It is last
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        } else {
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        }
      } else {
        theInsertSchemaObject = [];
      }
      console.log("Current schema");
      console.log(theInsertSchemaObject);


    })

    //Save the collection to be shown as button and fieldsOfOnsert that will be used on inserting object
    this.setState({ collections: theInsertSchemaObject, fieldsOfOnsert: cuurrentFields })
  }

  /**
   * Step 4
   * Processes received records from firebase
   * @param {Object} records
   */
  processRecords(records) {

    var fields = {};
    var arrays = {};
    var elements = [];
    var elementsInArray = [];
    var newState = {};
    var directValue = "";
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.elements = elements;
    newState.directValue = directValue;
    newState.elementsInArray = elementsInArray;
    newState.records = null;

    this.setState(newState);

    //Each display is consisted of
    //Fields   - This are string, numbers, photos, dates etc...
    //Arrays   - Arrays of data, ex items:[0:{},1:{},2:{}...]
    //         - Or object with prefixes that match in array
    //Elements - Object that don't match in any prefix for Join - They are represented as buttons.

    //In FireStore
    //GeoPoint
    //DocumentReference

    //If record is of type array , then there is no need for parsing, just directly add the record in the arrays list

    console.log(Common.getClass(records));
    if (Common.getClass(records) == "Array") {
      //Get the last name
      console.log("This is array");
      var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
      var allPathItems = subPath.split("+");
      console.log(allPathItems)
      if (allPathItems.length > 0) {
        var lastItem = allPathItems[allPathItems.length - 1];
        console.log(lastItem);
        arrays[lastItem] = records;

      }
      //this.setState({"arrays":this.state.arrays.push(records)})
    } else if (Common.getClass(records) == "Object") {
      //Parse the Object record
      for (var key in records) {
        if (records.hasOwnProperty(key)) {
          var currentElementClasss = Common.getClass(records[key]);
          console.log(key + "'s class is: " + currentElementClasss);

          //Add the items by their type
          if (currentElementClasss == "Array") {
            //Add it in the arrays  list
            arrays[key] = records[key];
          } else if (currentElementClasss == "Object") {
            //Add it in the elements list
            var isElementMentForTheArray = false; //Do we have to put this object in the array
            for (var i = 0; i < Config.adminConfig.prefixForJoin.length; i++) {
              if (key.indexOf(Config.adminConfig.prefixForJoin[i]) > -1) {
                isElementMentForTheArray = true;
                break;
              }
            }

            var objToInsert = records[key];
            //alert(key);
            objToInsert.uidOfFirebase = key;

            if (isElementMentForTheArray) {
              //Add this to the merged elements
              elementsInArray.push(objToInsert);
            } else {
              //Add just to elements
              elements.push(objToInsert);
            }

          } else if (currentElementClasss != "undefined" && currentElementClasss != "null") {
            //This is string, number, or Boolean
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "GeoPoint") {
            //This is GeoPOint
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "DocumentReference") {
            //This is DocumentReference
            //Add it to the fields list
            fields[key] = records[key];
          }

        }
      }
    } if (Common.getClass(records) == "String") {
      console.log("We have direct value of string");
      directValue = records;
    }

    //Convert fields from object to array
    var fieldsAsArray = [];
    console.log("Add the items now inside fieldsAsArray");
    console.log("Current schema");
    console.log(this.state.currentInsertStructure)
    //currentInsertStructure
    var keysFromFirebase = Object.keys(fields);
    console.log("keysFromFirebase")
    console.log(keysFromFirebase)
    var keysFromSchema = Object.keys(this.state.currentInsertStructure || {});
    console.log("keysFromSchema")
    console.log(keysFromSchema)

    keysFromSchema.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        var indexOfElementInFirebaseObject = keysFromFirebase.indexOf(key);
        if (indexOfElementInFirebaseObject > -1) {
          keysFromFirebase.splice(indexOfElementInFirebaseObject, 1);
        }
      }
    });

    console.log("keysFromFirebase")
    console.log(keysFromFirebase)

    //pp_add
    var navigation = Config.navigation;
    var itemFound = false;
    var showFields = null;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].editFields && navigation[i].link == "athlete") {
        showFields = navigation[i].editFields;
        itemFound = true;
      }
    }
    var pos = -1;

    //special processing for document_path
    if (records.document_path && records.document_path != "") {
      this.state.docDir = records.document_path;
    }
    else
      newState.docDir = this.state.club_id + "/" + uuidv1();
    //end pp_add

    keysFromFirebase.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        if (showFields)
          pos = showFields.indexOf(key);
        if (pos < 0)
          return;
        // fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        fieldsAsArray.splice(pos, 0, { "theKey": key, "value": fields[key] })
      }
    });



    //Get all array names
    var arrayNames = [];
    Object.keys(arrays).forEach((key) => {
      arrayNames.push(key)
    });

    var newState = {};
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.isJustArray = Common.getClass(records) == "Array";
    newState.elements = elements;
    newState.elementsInArray = elementsInArray;
    newState.directValue = directValue;
    newState.records = records;
    newState.isLoading = false;

    console.log("THE elements")
    console.log(elements);

    //Set the new state
    this.setState(newState);

    //Additional init, set the DataTime, check format if something goes wrong
    window.additionalInit();
  }

  /**
   *
   * Create R Update D
   *
   */

  /**
  * processValueToSave  - helper for saving in Firestore , converts value to correct format
  * @param {value} value
  * @param {type} type of field
  */
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }

  /**
  * updatePartOfObject  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {String} firebasePath current firestore path
  * @param {String} byGivvenSubLink force link to field
  * @param {Function} callback function after action
  */
  updatePartOfObject(key, value, dorefresh = false, type = null, firebasePath, byGivvenSubLink = null, callback = null) {
    var subLink = this.state.theSubLink;
    if (byGivvenSubLink != null) {
      subLink = byGivvenSubLink;
    }
    console.log("Sub save " + key + " to " + value + " and the path is " + firebasePath + " and theSubLink is " + subLink);
    var chunks = subLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    var _this = this;
    //First get the document
    //DOCUMENT - GET FIELDS && COLLECTIONS
    var docRef = firebase.app.firestore().doc(firebasePath);
    docRef.get().then(function (doc) {
      if (doc.exists) {
        var numChunks = chunks.length - 1;
        var doc = doc.data();
        //("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {        
        if (value == "DELETE_VALUE") {
          if (numChunks == 2) {
            if (chunks[1] == "attachments") //special treatment for attachments
            {
              var docPath = doc[chunks[1]][chunks[2]];
              docPath = unescape(docPath.slice(1 + docPath.lastIndexOf('/'), docPath.lastIndexOf('?')));
              var dRef = firebase.app.storage().ref().child(docPath);

              // Delete the file
              dRef.delete().then(someval => {
                console.log("Deleted from storage file: ", docPath);
              }).catch(function (error) {
                console.log("Could not delete storage file: " + error);
              });

            }//end special treatment for attachments
            doc[chunks[1]].splice(chunks[2], 1);
          }
          if (numChunks == 1) {
            doc[chunks[1]] = null;
          }
        } else {
          //Normal update, or insert
          if (numChunks == 2) {
            doc[chunks[1]][chunks[2]] = value
          }
          if (numChunks == 1) {
            doc[chunks[1]][key] = value
          }
        }

        console.log("Document data:", doc);
        _this.updateAction(chunks[1], doc[chunks[1]], dorefresh, null, true)
        if (callback) {
          callback();
        }

        //alert(chunks.length-1);
        //_this.processRecords(doc.data())
        //console.log(doc);

      } else {
        console.log("No such document!");
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });

  }

  //to save the fields edited in edit page
  saveEdits() {
    var firebasePath = (this.props.route.path.replace("/athlete/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    var db = firebase.app.firestore();

    //var databaseRef = db.doc(firebasePath);
    var databaseRef = db.doc(this.state.firebasePath);
    var updateObj = {};
    var i;
    for (i = this.state.editedFields.length - 1; i >= 0; --i) {
      updateObj[this.state.editedFields[i]] = this.state[this.state.editedFields[i]];
      this.state.editedFields.splice(i, 1);
    }
    databaseRef.set(updateObj, { merge: true });

  }

  resetEdits() {
    this.state.editedFields.splice(0, this.state.editedFields.length);
  }

  /**
  * Firebase update based on key / value,
  * This function also sets derect name and value
  * @param {String} key
  * @param {String} value
  */


  /**
  * updateAction  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {Boolean} forceObjectSave force saving sub object
  */
  updateAction(key, value, dorefresh = true, type = null, forceObjectSave = false) {
    const pattern = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/athlete/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {
        if (forceObjectSave) //Maybe coming from a delete action
        {
          var db = firebase.app.firestore();

          //special treatment for attachments. Don't zero out the array
          if (key == "attachments" && !Array.isArray(value))
            value = [];

          var databaseRef = db.doc(this.state.firebasePath);
          var updateObj = {};
          updateObj[key] = value;
          databaseRef.set(updateObj, { merge: true });
        }
        else //PP: DONT UPDATE THE DB HERE. WAIT FOR THE SAVE
        {

          if (!this.state.editedFields.includes(key))
            this.state.editedFields.push(key);



          if (key == "date_of_birth") {
            this.state.selectedEvents = [];
            this.state.events = "";
            this.state.swimGroup = this.getGroup(value);
            this.state.dobEntered = true;
            this.getEventList();

          }

          //Checkbox handling
          if(key == "events"){
          if(typeof(value) == "string")
            this.state.selectedEvents = value.slice(1).split(",");
          else
          {
            this.state.selectedEvents = [];
            value = "";
          }
        }

          this.setState({ [key]: value });
        }
      }

    }
  }

  /**
  * addDocumentToCollection  - used recursivly to add collection's document's collections
  * @param {String} name name of the collection
  * @param {FirestoreReference} reference
  */
  addDocumentToCollection(name, reference = null) {

    var pathChunks = this.state.firebasePath.split("/");
    pathChunks.pop();
    var withoutLast = pathChunks.join("/");
    console.log(name + " vs " + withoutLast);
    //Find the fields to be inserted
    var theInsertSchemaObject = INSERT_STRUCTURE[name].fields;
    console.log(JSON.stringify(theInsertSchemaObject));

    //Find the collections to be inserted
    var theInsertSchemaCollections = INSERT_STRUCTURE[name].collections;
    console.log(JSON.stringify(theInsertSchemaCollections));

    //Reference to root firestore or existing document reference
    var db = reference == null ? (pathChunks.length > 1 ? firebase.app.firestore().doc(withoutLast) : firebase.app.firestore()) : reference;

    //Check type of insert
    var isTimestamp = Config.adminConfig.methodOfInsertingNewObjects == "timestamp"

    //Create new element
    var newElementRef = isTimestamp ? db.collection(name).doc(Date.now()) : db.collection(name).doc()

    //Add data to the new element
    //newElementRef.set(theInsertSchemaObject)

    //Go over sub collection and insert them
    for (var i = 0; i < theInsertSchemaCollections.length; i++) {
      this.addDocumentToCollection(theInsertSchemaCollections[i], newElementRef)
    }


    //Show the notification on root element
    if (reference == null) {
      this.cancelAddFirstItem();
      this.setState({ notifications: [{ type: "success", content: "Element added. You can find it in the table bellow." }] });
      this.refreshDataAndHideNotification();
    }
  }

  /**
  * addKey
  * Adds key in our list of fields in firestore
  */
  addKey() {
    if (this.state.NAME_OF_THE_NEW_KEY && this.state.NAME_OF_THE_NEW_KEY.length > 0) {

      if (this.state.VALUE_OF_THE_NEW_KEY && this.state.VALUE_OF_THE_NEW_KEY.length > 0) {

        this.setState({ notifications: [{ type: "success", content: "New key added." }] });
        this.updateAction(this.state.NAME_OF_THE_NEW_KEY, this.state.VALUE_OF_THE_NEW_KEY);
        this.refs.simpleDialog.hide();
        this.refreshDataAndHideNotification();
      }
    }
  }

  /**
  * addItemToArray  - add item to array
  * @param {String} name name of the array
  * @param {Number} howLongItIs count of items, to know the next index
  */
  addItemToArray(name, howLongItIs) {

    //temp disabler
    return;

    console.log("Add item to array " + name);
    console.log("Is just array " + this.state.isJustArray);

    console.log("Data ");
    console.log(this.state.fieldsOfOnsert);

    var dataToInsert = null;
    var correctPathToInsertIn = "";
    if (this.state.fieldsOfOnsert) {
      if (this.state.isJustArray) {
        console.log("THIS IS Array")
        dataToInsert = this.state.fieldsOfOnsert[0];
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      } else {
        dataToInsert = this.state.fieldsOfOnsert[name];
        dataToInsert = dataToInsert ? dataToInsert[0] : null;
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + name + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      }
    }

    console.log("Data to insert");
    console.log(dataToInsert);
    console.log("Path to insert");
    console.log(correctPathToInsertIn);

    var _this = this;
    this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", dataToInsert, true, null, this.state.firebasePath, correctPathToInsertIn, function (e) {
      _this.setState({ notifications: [{ type: "success", content: "New element added." }] });
      _this.refreshDataAndHideNotification();
    })
  }

  /**
  *
  * C Read U D
  *
  */

  /**
  * showSubItems - displays sub object, mimics opening of new page
  * @param {String} theSubLink , direct link to the sub object
  */
  showSubItems(theSubLink) {
    var chunks = theSubLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    this.setState({
      itemOfInterest: chunks[1],
      theSubLink: theSubLink,
    })
    var items = this.state.records;
    for (var i = 1; i < chunks.length; i++) {
      console.log(chunks[i]);
      items = items[chunks[i]];
    }
    console.log("--- NEW ITEMS ");
    console.log(items)
    this.processRecords(items);
  }

  /**
  *
  * C R U Delete
  *
  */

  /**
  * deleteFieldAction - displays sub object, mimics opening of new page
  * @param {String} key to be updated
  * @param {Boolean} isItArrayItem 
  * @param {String} theLink 
  */
  deleteFieldAction(key, isItArrayItem = false, theLink = null) {
    console.log("Delete " + key);
    console.log(theLink);
    if (theLink != null) {
      theLink = theLink.replace("/athlete", "");
    }
    if (isNaN(key)) {
      isItArrayItem = false;
    }

    //special treatment when deleting attachments
    if (isItArrayItem)
      this.state.deleteAlertTitle = "This attachment will be deleted permanently";
    else
      this.state.deleteAlertTitle = "All data associated with this athlete will be deleted!";

    console.log("Is it array: " + isItArrayItem);
    var firebasePathToDelete = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (key != null) {
      //firebasePathToDelete+=("/"+key)
    }

    console.log("firebasePath for delete:" + firebasePathToDelete);
    this.setState({ deleteAlert: true, pathToDelete: theLink ? theLink : firebasePathToDelete, isItArrayItemToDelete: isItArrayItem, keyToDelete: theLink ? "" : key });

  }

  /**
  * doDelete - do the actual deleting based on the data in the state
  */
  doDelete() {
    var _this = this;

    var completeDeletePath = this.state.pathToDelete + "/" + this.state.keyToDelete;

    if (this.state.pathToDelete.indexOf(Config.adminConfig.urlSeparatorFirestoreSubArray) > -1) {
      //Sub data
      // _this.refs.deleteDialog.hide();
      this.hideAlert("deleteAlert");
      this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {
        _this.setState({ notifications: [{ type: "success", content: "Element deleted." }] });
        _this.refreshDataAndHideNotification();
      })
    } else {
      //Normal data

      var chunks = completeDeletePath.split("/");

      var db = firebase.app.firestore();


      if (chunks.length % 2) {
        //odd
        //Delete fields from docuemnt
        var refToDoc = db.doc(this.state.pathToDelete);

        // Remove the 'capital' field from the document
        var deleteAction = {};
        deleteAction[this.state.keyToDelete] = firebaseREF.firestore.FieldValue.delete();
        var removeKey = refToDoc.update(deleteAction).then(function () {
          console.log("Document successfully deleted!");
          //_this.refs.deleteDialog.hide();

          _this.setState({ deleteAlert: false, keyToDelete: null, pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });
      } else {
        //even
        //delete document from collection
        //alert("Delete document "+completeDeletePath);
        //db.collection(this.state.pathToDelete).doc(this.state.keyToDelete).delete().then(function() {
        db.collection(this.state.firebasePath).doc(this.state.keyToDelete).delete().then(function () {
          console.log("Document successfully deleted!");
          //_this.refs.deleteDialog.hide();

          _this.setState({ deleteAlert: false, pathToDelete: null, notifications: [{ type: "success", content: "Athlete deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing athlete: ", error);
        });

      }

    }




    /*firebase.database().ref(this.state.pathToDelete).set(null).then((e)=>{
      console.log("Delete res: "+e)
      this.refs.deleteDialog.hide();
      this.setState({keyToDelete:null,pathToDelete:null,notifications:[{type:"success",content:"Field is deleted."}]});
      this.refreshDataAndHideNotification();

    })*/
  }

  /**
  * cancelDelete - user click on cancel
  */
  cancelDelete() {
    console.log("Cancel Delete");
    this.refs.deleteDialog.hide()
  }

  cancelAddFirstItem() {
    console.log("Cancel Add");
    this.refs.addCollectionDialog.hide()
  }



  /**
  *
  * UI GENERATORS
  *
  */

  /**
  * This function finds the headers for the current menu
  * @param firebasePath - we will use current firebasePath to find the current menu
  */
  findHeadersBasedOnPath(firebasePath) {
    var headers = null;

    var itemFound = false;
    var navigation = Config.navigation;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
    //  if (navigation[i].path == "athletes" && navigation[i].tableFields && navigation[i].link == "AthleteAdmin") {

      if (navigation[i].tableFields) {
        headers = navigation[i].tableFields;
        itemFound = true;
      }

      //Look into the sub menus
      if (navigation[i].subMenus) {
        for (var j = 0; j < navigation[i].subMenus.length; j++) {
          //if (navigation[i].subMenus[j].path == firebasePath && navigation[i].subMenus[j].tableFields && navigation[i].subMenus[j].link == "assoc") {
          if (navigation[i].subMenus[j].tableFields) {
            headers = navigation[i].subMenus[j].tableFields;
            itemFound = true;
          }
        }
      }
    }
    return headers;
  }

  //checking whether coach is entered before entering 1st athlete
  prepAddNew() {
      this.refs.addNewAthleteDialog.show();
  }
  /**
  * makeCollectionTable
  * Creates single collection documents
  */
  //collection of athlete in first screen 
  makeCollectionTable() {
    var name = this.state.currentCollectionName;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          {/*begin pp_add*/}
          {/*
                  <a  onClick={()=>{this.addDocumentToCollection(name)}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}> */}
          <a style={{ cursor: "pointer" }} onClick={this.prepAddNew}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            {/*end pp_add*/}

            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">Athlete</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={true}
                name={name}
                routerPath={this.props.route.path}
                isJustArray={false}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.documents}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates single array section
   * @param {String} name, used as key also
   */
  //generate docs table in edit page
  // makeArrayCard(name) {
  //   return (
  //     <div className="col-md-12" key={name}>
  //       <div className="card">
  //         <div className="card-header card-header-icon" data-background-color="rose">
  //           <i className="material-icons">assignment</i>
  //         </div>
  //         <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addattachments.show(); }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
  //           <i className="material-icons">add</i>
  //         </div></a>
  //         <div className="card-content">
  //           <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
  //           <div className="toolbar">

  //           </div>
  //           <div className="material-datatables">
  //             <DocsTable
  //               isFirestoreSubArray={true}
  //               showSubItems={this.showSubItems}
  //               //headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
  //               headers={["name"]}
  //               deleteFieldAction={this.deleteFieldAction}
  //               fromObjectInArray={false} name={name}
  //               routerPath={this.props.route.path}
  //               isJustArray={this.state.isJustArray}
  //               sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
  //               data={this.state.arrays[name]} />
  //           </div>
  //         </div>
  //       </div>
  //     </div>
  //   )
  // }

  /**
   * Creates  table section for the elements object
   * @param {String} name, used as key also
   */
  // generate elements in edit page
  makeTableCardForElementsInArray() {
    var name = this.state.lastPathItem;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)} deleteFieldAction={this.deleteFieldAction} fromObjectInArray={true} name={name} routerPath={this.props.route.path} isJustArray={this.state.isJustArray} sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""} data={this.state.elementsInArray}>
              </Table>
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
    * Creates direct value section
    * @param {String} value, valu of the current path
    */
  makeValueCard(value) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <div className="card-content">
            <h4 className="card-title">Value</h4>
            <div className="toolbar">
            </div>
            <div>
              <Input updateAction={this.updateAction} className="" theKey="DIRECT_VALUE_OF_CURRENT_PATH" value={value} />
            </div>
          </div>
        </div>
      </div>
    )
  }


  /**
   * generateBreadCrumb
   */
  generateBreadCrumb() {
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    var path = "/athlete/"
    return (<div>{items.map((item, index) => {
      if (index == 0) {
        path += item;
      } else {
        path += "+" + item;
      }

      return (<Link className="navbar-brand" to={path}>{item} <span className="breadcrumbSeparator">{index == items.length - 1 ? "" : "/"}</span><div className="ripple-container"></div></Link>)
    })}</div>)
  }

  /**
   * generateNotifications
   * @param {Object} item - notification to be created
   */
  generateNotifications(item) {
    return (
      <div className="col-md-12">
        <Notification type={item.type} >{item.content}</Notification>
      </div>
    )
  }

  /**
  * refreshDataAndHideNotification
  * @param {Boolean} refreshData 
  * @param {Number} time 
  */
  refreshDataAndHideNotification(refreshData = true, time = 3000) {
    //Refresh data,
    if (refreshData) {
      this.resetDataFunction();
    }

    //Hide notifications
    setTimeout(function () { this.setState({ notifications: [] }) }.bind(this), time);
  }


  formValueCapture(k, v) {
    //alert(k);
    this.setState({ [k]: v });
  }


  cancelAddNewAthlete() {
      var newState = {};
      newState.full_name = "";
      newState.date_of_birth = "";
      newState.gender = "";
      newState.mobile_number = "";
      newState.email = "";
      newState.parent_name = "";
      newState.parent_mobile_number = "";
      newState.address = "";
      newState.city = "";
      newState.pin_code = "";
      newState.school_name = "";
      newState.school_code = "";
      newState.school_mobile_number = "";
      newState.school_email = "";
      newState.club_name = ""; 
      newState.swimGroup = "";   
      newState.selectedEvents = [];
      this.setState(newState);

    this.refs.addNewAthleteDialog.hide();
  }
  //validation for inputs
  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    if (name == "mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }

    if (name == "parent_mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }

    if (name == "school_mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    this.setState({
      [name]: value
    });
  }


  //addition of athlete for database
  addNewAthlete(e) {
    e.preventDefault(); // <- prevent form submit from reloading the page

    /* Send the message to Firebase */

    var db = firebase.app.firestore();
   
    if(this.state.date_of_birth == "" ){
      this.state.errorStatement = "Select Your Date Of Birth";
      this.setState({errorAlert: true})
      return;
    }

    if(this.state.gender === "" ){
      this.state.errorStatement = "Please ensure you have selected a gender";
      this.setState({errorAlert: true})
      return;
    }

    if(this.state.mobile_number.length < 10 ){
      this.state.errorStatement = "Please ensure you have entered a 10 digit mobile number";
      this.setState({errorAlert: true})
      return;
    }

    if(this.state.selectedEvents.length < 1 ){
      this.state.errorStatement = "Select atleast one event";
      this.setState({errorAlert: true})
      return;
    }

    if(this.state.selectedEvents.length >= 4 ){
      this.state.errorStatement = "You can select a maximum of 3 events";
      this.setState({errorAlert: true})
      return;
    }

 
    if (!this.state.debounce) {
      this.state.debounce = true;

      db.collection("aquaticsID/India/brands/icse/athletes").add({
        full_name: this.state.full_name,
        date_of_birth: this.state.date_of_birth,
        gender: this.state.gender,
        mobile_number: this.state.mobile_number,
        email: this.state.email,
        parent_name: this.state.parent_name,
        parent_mobile_number: this.state.parent_mobile_number,
        address: this.state.address,
        city: this.state.city,
        pin_code: this.state.pin_code,
        school_name: this.state.school_name,
        school_code: this.state.school_code,
        school_mobile_number: this.state.school_mobile_number,
        school_email: this.state.school_email,
        club_name: this.state.club_name,
        swimGroup: this.state.swimGroup,
        selected_events: this.state.selectedEvents.slice()
      })
        .then(docRef => {
          this.setState({ debounce: false, notifications: [{ type: "success", content: "New Athlete Added." }] })
          this.refs.addNewAthleteDialog.hide()
          this.refs.addCollectionDialog.hide()
          this.refreshDataAndHideNotification()

        })
        .catch(function (error) {
          alert("Error adding document: " + error);
        });

    }
  }
  //MAIN RENDER FUNCTION 
  render() {
    //styling for add new athlete skylight
    var addNewAthleteDialog = {
      width: '70%',
      height: 'auto',
      marginLeft: '-35%',
      position: 'absolute',
      padding: '0px'
    };
    //styling for add attachments skylight
    var addattactments = {
      width: '40%',
      height: 'auto',
      position: 'absolute',
      marginLeft: '-20%',
      padding: '0px'
    };

    return (
      <div className="content">
        <NavBar>
          {/* <div className="pull-right float-right">
            <a href="http://karnatakaswimming.org" target="_blank"> <img className="img-circle" src='assets/img/ksa_logo.png' height="70" width="80" /></a>
          </div> */}
        </NavBar>


        <div className="content" sub={this.state.lastSub}>


          <div className="container-fluid">

            <div style={{ textAlign: 'center' }}>
              {/* LOADER */}
              {this.state.isLoading ? <Loader color="#8637AD" size="12px" margin="4px" /> : ""}
            </div>

            {/* NOTIFICATIONS */}
            {this.state.notifications ? this.state.notifications.map((notification) => {
              return this.generateNotifications(notification)
            }) : ""}

            {/* sweet phone alert */}
            <SweetAlert
              warning
              show={this.state.phoneAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ phoneAlert: false })}>
              Please ensure you have selected a gender and have entered a 10 digit mobile number and a 6 digit PIN code!
              </SweetAlert>

            {/* sweet photo loading alert */}
            <SweetAlert
              show={this.state.loadingAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ loadingAlert: false })}>
              Please wait! Files/Images are uploading...
              </SweetAlert>

            {/* sweet delete alert */}
            <SweetAlert
              warning
              showCancel
              show={this.state.deleteAlert}
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="deleteAlertBtnColor"
              cancelBtnBsStyle="default"
              title="Are you sure?"
              onConfirm={() => this.doDelete()}
              onCancel={() => this.setState({ deleteAlert: false })}
            >
              {this.state.deleteAlertTitle}
            </SweetAlert>

              {/* Must select atleat one athlete */}
              <SweetAlert warning
              show={this.state.errorAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ errorAlert: false })}>
              {this.state.errorStatement}
              </SweetAlert>

            {/* Documents in collection */}
            {this.state.isCollection && this.state.documents.length > 0 ? this.makeCollectionTable() : ""}

            {/* DIRECT VALUE */}
            {this.state.directValue && this.state.directValue.length > 0 ? this.makeValueCard(this.state.directValue) : ""}

            {/* ARRAYS */}
            {this.state.arrayNames ? this.state.arrayNames.map((key) => {
              return this.makeArrayCard(key)
            }) : ""}
            {/* FIELDS */}
            {/* Fields in edit page */}
            {this.state.fieldsAsArray && this.state.fieldsAsArray.length > 0 ? (<div className="col-md-12">
              <div className="card">
                {/*
                <a  onClick={()=>{this.refs.simpleDialog.show()}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}>
                    <i className="material-icons">add</i>
                </div></a>*/}
                 <form className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{Common.capitalizeFirstLetter(Config.adminConfig.fieldBoxName)}</h4>
                  </div>
                  {this.state.fieldsAsArray ? this.state.fieldsAsArray.map((item) => {

                    return (
                      <Fields
                        isFirestore={true}
                        parentKey={null}
                        key={item.theKey + this.state.lastSub}
                        deleteFieldAction={this.deleteFieldAction}
                        updateAction={this.updateAction}
                        theKey={item.theKey}
                        value={item.value} />)


                  }) : ""}
                  <div
                    className="text-center">

                    <Link to='/athlete'>
                      <button onClick={this.saveEdits}
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Save">Save</button>
                    </Link>

                    <Link to='/athlete'>
                      <button
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Cancel">Cancel</button>
                    </Link>

                  </div>

                </form>
              </div>
            </div>) : ""}


            {/* COLLECTIONS */}
            {this.state.theSubLink == null && this.state.isDocument && this.state.collections && this.state.collections.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{"Collections"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.theSubLink == null && this.state.collections ? this.state.collections.map((item) => {
                      var theLink = "/athlete/" + this.state.completePath + Config.adminConfig.urlSeparator + item;
                      return (<Link to={theLink}><a className="btn">{item}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}



            {/* ELEMENTS MERGED IN ARRAY */}
            {this.state.elementsInArray && this.state.elementsInArray.length > 0 ? (this.makeTableCardForElementsInArray()) : ""}

            {/* ELEMENTS */}
            {this.state.elements && this.state.elements.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{this.state.lastPathItem + "' elements"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.elements ? this.state.elements.map((item) => {
                      var theLink = "/fireadmin/" + this.state.completePath + Config.adminConfig.urlSeparatorFirestoreSubArray + item.uidOfFirebase;
                      return (<Link onClick={() => { this.showSubItems(theLink) }}><a className="btn">{item.uidOfFirebase}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>
                </form>
              </div>
            </div>) : ""}
          </div>
        </div>
        {/* Skylight for adding first athlete */}
        <SkyLight hideOnOverlayClicked ref="addCollectionDialog" title="">
          <span><h3 className="center-block">Add the first athlete in your club</h3></span>
          <div className="col-md-12">
            <Notification type="success" >Looks like there are no athletes yet. Add your first athlete.</Notification>
          </div>

          {/* <div className="col-md-12">
            Data Location
          </div>
          <div className="col-md-12">
            <b>{this.state.showAddCollection}</b>
          </div> */}


          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelAddFirstItem} className="btn btn-info center-block">Cancel</a>
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.prepAddNew} className="btn btn-success center-block">ADD</a>
            </div>

          </div>

        </SkyLight>
        {/*End Skylight for adding first athlete */}

        {/* Skylight for adding new athlete */}
        {/*begin pp_add*/}
        <SkyLight dialogStyles={addNewAthleteDialog} ref="addNewAthleteDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Register Athlete</h4>

              <div className="col-sm-12 common-padding">
              <div className="col-sm-12">
                <p className="margin-0px"><b>Venue: </b>Nettakallappa Aquatic Centre, #47/1/40.Uttarahalli Main,Road, Bangalore:560061</p>
                <p className="margin-0px"><a target="_blank" className="linkStyling text-color" href="https://goo.gl/maps/keGi4dVokVz"><b>Location Map</b></a></p>
                <p className="margin-0px"><b>Dates: </b> 29 August, 2018</p>
                <p>Visit <a target="_blank" className="linkStyling" href="http://swimindia.in/icse-school-swimming-competition-2018-bangalore">SwimIndia</a> to know more about the meet.</p>
              </div>
            </div>
              <form className="padding-10" onSubmit={this.addNewAthlete}>
                <div className="row">
                 {/* Full name */}
                 <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Swimmer Name <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text" required="true" name="full_name" aria-required="true" value={this.state.full_name} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Full name */}
                  {/* Date Of Birth */}
                 <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Date Of Birth <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                         <KreedDatePicker theKey="date_of_birth" required="true" value={this.state.date_of_birth} updateAction={this.updateAction} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Date Of Birth */}
                  {/* gender */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Gender <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating margin-top-10">
                         <Radio theKey="gender" updateAction={this.updateAction} value={this.state.gender} options={["male", "female"]} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* gender */}
                  {/* Mobile Number */}
                 <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Phone <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text" name="mobile_number" required="true" value={this.state.mobile_number} onChange={this.handleInputChange} className="form-control" />  
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Mobile Number */}
                  {/* Email */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Email <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="email" required="true" name="email" aria-required="true" value={this.state.email} onChange={this.handleInputChange} className="form-control" />  
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Email */}
                  {/* Parent/Guardian Name */}
                    <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Parent/Guardian Name <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text" required="true" name="parent_name" aria-required="true" value={this.state.parent_name} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Parent/Guardian Name */}
                  {/* Parent/Guardian Number */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Parent/Guardian Contact Number </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text" name="parent_mobile_number" value={this.state.parent_mobile_number} onChange={this.handleInputChange} className="form-control" /> 
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Parent/Guardian Number */}
                 {/* Address */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Address </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                         <textarea name="address" value={this.state.address} onChange={this.handleInputChange} className="form-control" rows="4" cols="21" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Address */} 
                  {/* City */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">City </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text"  name="city" value={this.state.city} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* City */}
                  {/* Pincode */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Pin Code </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                        <input type="text" name="pin_code" aria-required="true" value={this.state.pin_code} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Pincode */}
                  {/* school */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">School Name <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text" required="true" name="school_name" aria-required="true" value={this.state.school_name} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* school */}
                  {/* school Code */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">School Code </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text"  name="school_code" value={this.state.school_code} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* school code */}
                  {/* school phone number */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">School Contact Number </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text" name="school_mobile_number" value={this.state.school_mobile_number} onChange={this.handleInputChange} className="form-control" /> 
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* school phone number */}
                  {/* School email */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">School Email </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="email"  name="school_email"  value={this.state.school_email} onChange={this.handleInputChange} className="form-control" />  
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* school email */}
                  {/* Group and events  */}
                  {this.state.swimGroup != "" && <div>
                    <div className="row">
                    <div className="col-sm-4"></div>
                      <div className="col-sm-8">
                        <label className="control-label label-color"> {this.state.swimGroup}</label>
                      </div>

                    </div>
                    <div className="row">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color"> Select Events <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating ">
                          <CheckBox theKey="events" updateAction={this.updateAction} value={this.state.events} options={this.state.eventList} />
                        </div>
                      </div>
                    </div>
                    </div>
                   
                  </div>}
                  {/* Group and events */}
                  {this.state.selectedEvents.length >= 4 && <div>
                  <div className="row">
                    <div className="col-sm-6 text-align-right">
                      <label className="control-label color-red">You can only choose upto 3 choice(s).</label>
                    </div>
                  </div>
                  </div>}
                  {/* Club name */}
                  <div className="row margin-tb-15">
                    <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Club Name <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-6 margin-padding-0">
                      <div className="input-group display-block">
                        <div className="form-group label-floating">
                          <input type="text" required="true" name="club_name" aria-required="true" value={this.state.club_name} onChange={this.handleInputChange} className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Club name */}
                   {/* Terms and Conditions */}
                   <div className="row">
                   <div className="col-sm-4 text-align-right">
                      <label className="control-label label-color">Terms and Conditions <span className="color-red"> *</span> </label>
                    </div>
                    <div className="col-sm-8"></div>
                   </div>
                    <div className="row">
                    <div className="col-sm-1"></div>
                    <div className="col-sm-8 margin-left-40">
                      <div className="border-height">
                        <ol>
                        <label className="control-label label-color margin-tb-20"><b>Eligibility Criteria</b> </label>
                          <li>Only two participants each from the region in each category in each event.</li>
                          <li>A participant can compete only in 3 events excluding the Freestyle relay.</li>
                          <li>The costumes of all competitors and trainers shall be in good moral taste. They must be non-transparent.</li>
                          <li>The Referee has the authority to exclude any competitor, whose costume does not conform to this standard.</li>
                          <li>There shall be a Technical Committee to oversee the smooth conduct of the competition.</li>
                          <li>Duties of the Technical Committee:
                            <ul className="list-type">
                              <li>To ensure the rules for swimming and implement them as per the SFI (Swimming Federation of India).</li>
                              <li>To ensure the specifications of the pool as per the national standard.</li>
                              <li>To ensure that the participants have suitable swimming suit, chest numbers and the lanes for the competition.</li>
                              <li>To deal with any dispute or indiscipline during the event.</li>
                              <li>To provide emergency medical aid for the participants.</li>
                              <li>To ensure that the event is conducted by qualified officials.</li>
                              <li>To ensure the accurate distance for all the events in the respective disciplines.</li>
                              <li>To ensure that there are referees for each lane to avoid any disputes.</li>
                              <li>To ensure that the time keeper keeps the record of the timings clocked during the competition and displays them on the display board.</li>
                              <li>To ensure that disputes, if any, are submitted to the Jury of Appeal in the proper way.</li>
                            </ul>
                          </li>
                          <li>Disqualification, if any, can be made only by the official in-charge for a valid and grave reason.</li>
                          <li>A swimmer is not permitted to wear any device or use any substance, which may help his/her speed or buoyancy.</li>
                          <li>Goggles may be worn and rubdown oil applied for safety measures, if approved by the Referee.</li>
                          <li>The participants cannot grasp the lane dividers to assist his/her forward motion.</li>
                          <li>Officials:
                            <ul className="list-type">
                              <li>Referee</li>
                              <li>Time Keepers (lane wise)</li>
                              <li>Lane Judges.</li>
                              <li>Recorders</li>
                              <li>Starter</li>
                              <li>Clerk of Course</li>
                              <li>Stroke Judge with Turn Judges</li>
                              <li>Announcer</li>
                            </ul>
                          </li>
                          <li>Details of the events : Fifty and Hundred Metrs Free Style, Breast, Back and ButterFly strokes plus 4 into 50 Metrs FreeStyle relay.</li>
                        </ol>
                      </div>
                    </div>
                    </div>
                  
                    <div className="col-sm-6 text-align-right">
                      <input className="margin-5" type="checkbox" required="true" name="agree_TC" aria-required="true" value={this.state.agree_TC} onChange={this.handleInputChange}/>By clicking here , I agree to the terms & conditions 
                    </div>
                  {/* Terms and Conditions */}
                </div>
                <div className="row margin-top-25px">
                  <div className="col-sm-12 text-align-center">
                    <button className="btn btn-rose btn-size font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                    <button onClick={this.cancelAddNewAthlete} className="btn btn-rose btn-size font-size " value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>

        {/*end pp_add*/}
        {/*End Skylight for adding new athlete */}


       
 
 

        <SkyLight hideOnOverlayClicked ref="simpleDialog" title="">
          <span><h3 className="center-block">Add new key</h3></span>
          <br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Name of they key</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="NAME_OF_THE_NEW_KEY" value={"name"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div><br /><br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Value</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="VALUE_OF_THE_NEW_KEY" value={"value"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div>
          <div className="col-sm-12 ">
            <div className="col-sm-3 ">
            </div>
            <div className="col-sm-6 center-block">
              <a onClick={this.addKey} className="btn btn-rose btn-round center-block"><i className="fa fa-save"></i>   Add key</a>
            </div>
            <div className="col-sm-3 ">
            </div>
          </div>
        </SkyLight>
      </div>
    )
  }

}
export default AthleteAdmin;

